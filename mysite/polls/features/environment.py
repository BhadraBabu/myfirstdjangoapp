import datetime
import os
import django

from selenium.webdriver import DesiredCapabilities

os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'
django.setup()

from django.test.runner import DiscoverRunner
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from django.contrib.auth.models import User
from polls.models import Question
from django.utils import timezone

def before_all(context):
    context.test_runner = DiscoverRunner()
    context.test_runner.setup_test_environment()
    context.old_db_config = context.test_runner.setup_databases()
    user = User.objects.create_user(
        username='user01',
        password='user01pwd',
        email='user01@gmail.com'
     )
    user.save()



def after_all(context):
    User.objects.filter(username='user01').delete()
    context.test_runner.teardown_databases(context.old_db_config)
    context.test_runner.teardown_test_environment()



def before_scenario(context, scenario):
    context.test = StaticLiveServerTestCase
    context.test.setUpClass()
    desired_capabilities = DesiredCapabilities.CHROME

    # context.test.selenium = webdriver.Chrome()
    context.test.selenium = webdriver.Remote(command_executor="http://localhost:4444/wd/hub",
                                             desired_capabilities=desired_capabilities)
    context.test.selenium.implicitly_wait(10)


    if (scenario.name == 'user at home page when there are polls' or
            scenario.name == 'user click on the poll' or
            scenario.name == 'user vote for a poll'):
        time = timezone.now() + datetime.timedelta(-5)
        q = Question.objects.create(question_text='Whats your favourite colour?', pub_date=time)
        q.choice_set.create(choice_text="red", votes=0)
        q.choice_set.create(choice_text="blue", votes=0)


def after_scenario(context, scenario):
    Question.objects.filter(question_text='Whats your favourite colour?').delete()
    context.test.selenium.quit()
    context.test.tearDownClass()
    del context.test






