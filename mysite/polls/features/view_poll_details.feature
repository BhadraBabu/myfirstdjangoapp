# Created by bhadra at 22/01/20
Feature: View each poll in detail
  As a user
  I want to see each poll in detail
  So that I can see the different choices available


   Scenario: user click on the poll
    Given I logged in to the application
    When I click on the poll
    Then I see the different choices available