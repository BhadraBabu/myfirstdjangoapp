# Created by bhadra at 22/01/20
Feature: Vote for polls
  As a user
  I want a voting option
  So that I can submit my vote and see the results

  Scenario: user vote for a poll
    Given I logged in to the application
    When I click on the poll
    And I vote
    Then I see the results page