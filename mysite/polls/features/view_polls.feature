# Created by bhadra at 22/01/20
Feature: View the list of polls
  As a valid user
  I want to login to the polls app
  So that I can view different polls


  Background: Login
    Given I logged in to the application

  Scenario: user at home page when there are no polls available

    Then I see an error message
    """
    No Polls are available
    """

  Scenario: user at home page when there are polls

    Then I see the list of polls


