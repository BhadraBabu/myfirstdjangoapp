from behave import *
import json
import time

use_step_matcher("re")


@given("I navigate to the login page")
def step_impl(context):
    context.test.selenium.get('%s%s' % (context.test.live_server_url, '/accounts/login/'))
    context.test.selenium.save_screenshot("test.png")
    time.sleep(2)


@when("I login with a valid user")
def step_impl(context):
    # context.test.selenium.implicitly_wait(20)
    # print(context.test.selenium.current_url)
    # print(context.test.selenium.title)
    from selenium.webdriver.support.wait import WebDriverWait
    timeout = 2
    with open('polls/features/fixtures/valid_user.json') as f:
        data = json.load(f)
    username_input = context.test.selenium.find_element_by_name("username")
    username_input.send_keys(data["username"])
    password_input = context.test.selenium.find_element_by_name("password")
    password_input.send_keys(data["password"])
    context.test.selenium.find_element_by_id('login_button').click()

    time.sleep(2)


@when("I login with an invalid user")
def step_impl(context):
    with open('polls/features/fixtures/invalid_user.json') as f:
        data = json.load(f)
    username_input = context.test.selenium.find_element_by_name("username")
    username_input.send_keys(data["username"])
    password_input = context.test.selenium.find_element_by_name("password")
    password_input.send_keys(data["password"])
    time.sleep(2)
    context.test.selenium.find_element_by_id('login_button').click()
    time.sleep(2)


@then("I see a Home page")
def step_impl(context):
    time.sleep(5)
    context.test.selenium.save_screenshot("home.png")
    welcome_msg = context.test.selenium.find_element_by_id('polls_heading').text
    expected_msg = "Choose a Question"
    assert welcome_msg == expected_msg


@then('I see an error message')
def step_impl(context):
    context.test.selenium.save_screenshot("no_polls.png")
    if context.scenario.name == 'user at home page when there are no polls available':
        error_msg = context.test.selenium.find_element_by_id('polls_empty').text
    elif context.scenario.name == 'login with an invalid user':
        error_msg = context.test.selenium.find_element_by_id('login_error').text
    assert error_msg == context.text


@then("I see the list of polls")
def step_impl(context):
    poll = context.test.selenium.find_element_by_name('question_text').text
    assert poll is not None


@given("I logged in to the application")
def step_impl(context):
    context.execute_steps('''
    given I navigate to the login page
    ''')
    context.execute_steps(u'''
    when I login with a valid user
    ''')


@when("I click on the poll")
def step_impl(context):
    context.test.selenium.find_element_by_name('question_link').click()


@then("I see the different choices available")
def step_impl(context):
    question = context.test.selenium.find_element_by_id('question_text').text
    choice1 = context.test.selenium.find_elements_by_name('choice')[0].text
    assert choice1 is not None
    assert question is not None


@step("I vote")
def step_impl(context):
    context.test.selenium.find_element_by_name("choice").click()
    context.test.selenium.find_element_by_id('vote_button').click()


@then("I see the results page")
def step_impl(context):
    url = context.test.selenium.current_url
    assert 'results' in url
    assert context.test.selenium.find_element_by_id('question_text').text is not None
    assert context.test.selenium.find_element_by_name('choice').text is not None
