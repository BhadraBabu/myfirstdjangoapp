# Created by bhadra at 17/01/20
Feature: Login Page for Polls App
  As a user
  I want to login to the polls app
  So that I can check the login functionality

  Scenario: login with a valid user
    Given I navigate to the login page
    When I login with a valid user
    Then I see a Home page

   Scenario: login with an invalid user
    Given I navigate to the login page
    When I login with an invalid user
    Then I see an error message
    """
    Your username and password didn't match. Please try again.
    """



